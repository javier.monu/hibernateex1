/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package cat.copernic.exer1;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import com.github.javafaker.Faker;

/**
 *
 * @author javic
 */
public class Exer1 {
    private static SessionFactory factory;
    private static Session session;
    private static Logger logger;

    public static void main(String[] args) {
        try{
            factory = new Configuration().configure("cat/copernic/exer1/hiberConf/hibernate.cfg.xml").buildSessionFactory();
            logger = LogManager.getLogger(Exer1.class);
            Faker faker = new Faker();

            

            logger.info("session open");
            session = factory.openSession();

            logger.trace("Start transaction");
            session.getTransaction().begin();

            for (int i = 0; i < 1000; i++) {
                
                session.persist(new Stock("l"+i,faker.leagueOfLegends().champion()));
                logger.trace("Created Stock");
            }

            logger.info("Finished");
            session.getTransaction().commit();
            
        } catch (ConstraintViolationException ex){
             if (session.getTransaction() != null) 
                 session.getTransaction().rollback();
             logger.error("ViolaciÃ³ de la restricciÃ³: " + ex.getMessage());
            
        } catch (HibernateException ex) {
            if (session.getTransaction()!=null) 
                session.getTransaction().rollback();
            
            logger.error("ExcepciÃ³ d'hibernate: " + ex.getMessage());
        } catch (Exception ex){
            if (session.getTransaction() !=null) 
                   session.getTransaction().rollback();
               logger.error("ExcepciÃ³: " + ex.getMessage());
        }
        finally {
            session.close();
            
            
            factory.close();
        }
    }
}
