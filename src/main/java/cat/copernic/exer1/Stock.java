package cat.copernic.exer1;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Integer stockId;
    public Integer getStockId() {return stockId;}

    @Column(unique = true, nullable = false)
    private String stockCode;
    public String getStockCode() {return stockCode;}

    @Column(nullable = false)
    private String stockName;
    public String getStockName() {return stockName;}

    public Stock() {}

    

    public Stock(Integer stockId) {
        this.stockId = stockId;
    }

    public Stock(String stockCode, String stockName) {
        this.stockCode = stockCode;
        this.stockName = stockName;
    }
}
